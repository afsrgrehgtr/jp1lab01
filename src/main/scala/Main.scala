/*@main def mainProg: Unit = {
  println(napis)
}

val napis = "Hello"

@main
def prog2(n:Int):Unit={
  println(s"Dostałem wartość $n")
}

@main
def prog3(imie: String,wiek:Int,czy_gra:Boolean): Unit = {
  val granie = if czy_gra then {"gra"} else {"nie gra"}
  println(s"$imie ma $wiek lat i $granie w piłkę")
}
*/
@main
def prog4(n:Int):Unit = {
val rand = scala.util.Random()
val liczba = rand.nextInt(100)
println(s"Wylosowana liczba: $liczba")
//sprawdzamy czy n jest z zakresu 0-100
//jeśli tak, to sprawdzamy czy n jest mniejsze
//lub równe lub większe od wylosowanej liczby
if n >= 0 && n <= 100 then {
  if n < liczba then {
    println("n mniejsze od wylosowanej")
} else if n == liczba then {
    println("n jest rowne wylosowanej")
} else {
    println("n większe od wylosowanej")
}
}
}